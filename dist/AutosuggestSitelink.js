(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
/**
 * @class
 * @property {string} dbName The db name of the current wiki
 * @property {string} page The name of page
 * @property {string} wikidataUrl
 * @property {string} infoUrl Documentation page for this script
 * @property {Object} api The api object to query data from wikidata
 */
var AutosuggestSitelink = /*#__PURE__*/function () {
  function AutosuggestSitelink() {
    _classCallCheck(this, AutosuggestSitelink);
    this.dbName = mw.config.get('wgDBname');
    this.page = mw.config.get('wgPageName');
    this.title = mw.config.get('wgTitle');
    this.wikidataUrl = 'https://www.wikidata.org';

    // When on testwiki, use Test Wikidata.
    if (this.dbName === 'testwiki') {
      this.wikidataUrl = 'https://test.wikidata.org';
    }
    this.api = new mw.ForeignApi(this.wikidataUrl + '/w/api.php');
  }

  /**
   * Check if page has sitelinks related to this wiki,
   * and if it does, suggest possible sitelinks.
   */
  _createClass(AutosuggestSitelink, [{
    key: "checkSitelink",
    value: function checkSitelink() {
      var _this = this;
      this.api.get({
        format: 'json',
        action: 'wbgetentities',
        props: 'sitelinks/urls',
        sitefilter: this.dbName,
        sites: this.dbName,
        titles: this.page
      }).done(function (data) {
        // If "-1" key is not found in entities then the page has sitelinks.
        if (data.entities['-1'] !== undefined) {
          _this.loadTranslations().then(_this.suggestSiteLink.bind(_this));
        }
      });
    }

    /**
     * Not found, include link to create new item.
     *
     * @return {jQuery.Object}
     */
  }, {
    key: "itemsNotFound",
    value: function itemsNotFound() {
      var $container = $('<div>');
      $container.append($('<p>').text(mw.msg('asl-notfound')), $('<a>').attr('href', this.wikidataUrl + '/wiki/Special:NewItem?' + new URLSearchParams({
        site: this.dbName,
        page: this.page,
        label: this.title,
        lang: mw.config.get('wgContentLanguage')
      })).text(mw.msg('asl-createnewitem')).attr('target', '_blank').attr('style', 'margin-top: 30px;'));
      return $container;
    }

    /**
     * Find possible related wikidata items based on the page title.
     * If items are found, a dialog is shown with a submission form.
     */
  }, {
    key: "suggestSiteLink",
    value: function suggestSiteLink() {
      var _this2 = this;
      this.api.get({
        format: 'json',
        action: 'query',
        list: 'search',
        srsearch: this.title,
        srprop: 'snippet|titlesnippet'
      }).then(function (data) {
        if (data.query.searchinfo.totalhits === 0) {
          // No sitelinks at all.
          mw.notify(_this2.itemsNotFound(), {
            autoHideSeconds: 'long',
            tag: 'itemsNotFound'
          });
          return;
        }
        var Dialog = require('./Dialog.js');
        var dialogInstance = new Dialog(_this2, data.query.search);

        // Add the dialog to the window manager
        var windowManager = OO.ui.getWindowManager();
        windowManager.addWindows([dialogInstance]);
        windowManager.openWindow(dialogInstance);
      });
    }

    /**
     * Send a POST request to add the sitelink.
     *
     * @param {string} item
     * @return {jQuery.Promise}
     */
  }, {
    key: "submit",
    value: function submit(item) {
      return this.api.postWithToken('csrf', {
        action: 'wbsetsitelink',
        id: item,
        linksite: this.dbName,
        linktitle: this.page
      });
    }

    /**
     * Load the translations from the on-wiki messages page.
     *
     * @return {jQuery.Deferred}
     */
  }, {
    key: "loadTranslations",
    value: function loadTranslations() {
      var dfd = $.Deferred(),
        messagesPage = 'MediaWiki:Gadget-AutosuggestSitelink-messages',
        metaApi = new mw.ForeignApi('https://meta.wikimedia.org/w/api.php'),
        userLang = mw.config.get('wgUserLanguage'),
        langPageEn = "".concat(messagesPage, "/en"),
        langPageLocal = "".concat(messagesPage, "/").concat(userLang),
        titles = [langPageEn];
      if (mw.config.get('wgUserLanguage') !== 'en') {
        // Fetch the translation in the user's language, if not English.
        titles.push(langPageLocal);
      }
      var coreMessagesPromise = metaApi.loadMessagesIfMissing(['parentheses-start', 'parentheses-end']);
      var aslMessagesPromise = metaApi.get({
        action: 'query',
        prop: 'revisions',
        titles: titles,
        rvprop: 'content',
        rvslots: 'main',
        format: 'json',
        formatversion: 2
      }).then(function (resp) {
        var messagesLocal = {},
          messagesEn = {};
        resp.query.pages.forEach(function (page) {
          var parsedContent;
          if (page.missing) {
            return;
          }

          /**
           * The content model of the messages page is wikitext so that it can be used with
           * Extension:Translate. Consequently, it's easy to break things, so we do a
           * try/catch, try some commons fixes, and indicate the likely culprit to the user.
           */
          var content = page.revisions[0].slots.main.content;
          try {
            try {
              parsedContent = JSON.parse(content);
            } catch (_unused) {
              // A common failure reason is because of the HTML that's added for
              // untranslated messages, so remove this and try to parse again.
              // eslint-disable-next-line es-x/no-string-prototype-replaceall
              content = content.replaceAll('<span lang="en" dir="ltr" class="mw-content-ltr">', '');
              // eslint-disable-next-line es-x/no-string-prototype-replaceall
              content = content.replaceAll('</span>', '');
              parsedContent = JSON.parse(content);
            }
          } catch (_unused2) {
            // If it's still failing, there's something else wrong with the
            // messages (e.g. a double quote within a translated message).
            var metaLink = '<a href="https://meta.wikimedia.org/wiki/">' + page.title + '</a>';
            return OO.ui.alert($('<span>Unable to parse the messages page ' + metaLink + '. ' + 'There may have been a recent change that contains invalid JSON.</span>'), {
              title: 'Something went wrong'
            });
          }
          if (page.title === langPageLocal) {
            messagesLocal = parsedContent.messages;
          } else {
            messagesEn = parsedContent.messages;
          }
        });
        mw.messages.set(Object.assign({}, messagesEn, messagesLocal));
      });
      Promise.all([coreMessagesPromise, aslMessagesPromise]).then(function () {
        dfd.resolve();
      });
      return dfd;
    }
  }]);
  return AutosuggestSitelink;
}();
function init() {
  // Return when not in a content namespace or the article does not exist.
  var nsIds = mw.config.get('wgNamespaceIds');
  var validNamespaces = mw.config.get('wgContentNamespaces')
  // Remove File NS.
  .filter(function (e) {
    return e !== nsIds.file;
  });
  // Add Category NS.
  validNamespaces.push(nsIds.category);
  var ns = mw.config.get('wgNamespaceNumber');
  if (!validNamespaces.includes(ns) || mw.config.get('wgArticleId') === 0) {
    return;
  }
  Promise.all([
  // Resource loader modules
  mw.loader.using(['mediawiki.action.view.postEdit', 'mediawiki.ForeignApi', 'mediawiki.Title', 'oojs-ui-core', 'oojs-ui-windows'])]).then(function () {
    var autosuggestSitelink = new AutosuggestSitelink();
    autosuggestSitelink.checkSitelink();
  });
}
$.when(mw.loader.using(['mediawiki.util']), $.ready).then(function () {
  if (window.AutosuggestSitelinkDebug) {
    init();
  } else {
    mw.hook('postEdit').add(init);
  }
  var link = mw.util.addPortletLink('p-tb', '#', 'AutosuggestSiteLink', 'tb-autosuggestsitelink');
  $(link).on('click', function (e) {
    $(link).children().css({
      color: '#C4C2C1'
    });
    setTimeout(function () {
      $(link).children().css({
        color: ''
      });
    }, 3000);
    init();
    e.preventDefault();
  });
});

},{"./Dialog.js":2}],2:[function(require,module,exports){
"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _get() { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get.bind(); } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(arguments.length < 3 ? target : receiver); } return desc.value; }; } return _get.apply(this, arguments); }
function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }
function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); Object.defineProperty(subClass, "prototype", { writable: false }); if (superClass) _setPrototypeOf(subClass, superClass); }
function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }
function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }
function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }
function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }
function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }
function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }
function _classPrivateMethodInitSpec(obj, privateSet) { _checkPrivateRedeclaration(obj, privateSet); privateSet.add(obj); }
function _checkPrivateRedeclaration(obj, privateCollection) { if (privateCollection.has(obj)) { throw new TypeError("Cannot initialize the same private elements twice on an object"); } }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
function _classPrivateMethodGet(receiver, privateSet, fn) { if (!privateSet.has(receiver)) { throw new TypeError("attempted to get private field on non-instance"); } return fn; }
var _getItemsFieldset = /*#__PURE__*/new WeakSet();
/**
 * @class
 * @property {AutosuggestSitelink} aslContext
 * @property {Array<Object>} items
 */
var Dialog = /*#__PURE__*/function (_OO$ui$ProcessDialog) {
  _inherits(Dialog, _OO$ui$ProcessDialog);
  var _super = _createSuper(Dialog);
  /**
   * @param {AutosuggestSitelink} aslContext
   * @param {Array<Object>} items
   * @constructor
   */
  function Dialog(aslContext, _items) {
    var _this;
    _classCallCheck(this, Dialog);
    _this = _super.call(this);
    _classPrivateMethodInitSpec(_assertThisInitialized(_this), _getItemsFieldset);
    _this.aslContext = aslContext;
    _this.items = _items;
    _this.helpUrl = 'https://meta.wikimedia.org/wiki/Meta:AutosuggestSitelink';

    // These properties are set here instead of using the static keyword above
    //   because mw.msg() isn't available in the static context.
    Dialog.title = mw.msg('asl-popuptitle');
    Dialog.actions = [{
      action: 'submit',
      label: mw.msg('asl-submit'),
      flags: ['primary', 'progressive'],
      disabled: true
    }, {
      action: 'help',
      label: mw.msg('asl-help'),
      icon: 'helpNotice',
      href: _this.helpUrl
    }, {
      action: 'close',
      flags: ['safe', 'close']
    }];

    // Add properties needed by the ES5-based OOUI inheritance mechanism.
    // This roughly simulates OO.inheritClass()
    Dialog.parent = Dialog["super"] = OO.ui.ProcessDialog;
    OO.initClass(OO.ui.ProcessDialog);
    Dialog["static"] = Object.create(OO.ui.ProcessDialog["static"]);
    Object.keys(Dialog).forEach(function (key) {
      Dialog["static"][key] = Dialog[key];
    });
    return _this;
  }

  /**
   * @param {...*} args
   * @override
   */
  _createClass(Dialog, [{
    key: "initialize",
    value: function initialize() {
      var _get2;
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }
      (_get2 = _get(_getPrototypeOf(Dialog.prototype), "initialize", this)).call.apply(_get2, [this].concat(args));
      this.itemsFieldset = _classPrivateMethodGet(this, _getItemsFieldset, _getItemsFieldset2).call(this, this.items);
      this.panel = new OO.ui.PanelLayout({
        padded: true,
        expanded: false
      });
      this.panel.$element.append(this.itemsFieldset.$element);
      this.$body.append(this.panel.$element);
    }

    /**
     * Get the fieldset of items with radio buttons.
     *
     * @param {Array<Object>} items
     * @return {OO.ui.FieldsetLayout}
     */
  }, {
    key: "getActionProcess",
    value:
    /**
     * @param {string} action
     * @return {OO.ui.Process}
     * @override
     */
    function getActionProcess(action) {
      var _this2 = this;
      return _get(_getPrototypeOf(Dialog.prototype), "getActionProcess", this).call(this, action).next(function () {
        if (action === 'submit') {
          return _this2.aslContext.submit(_this2.itemSelect.findSelectedItem().data);
        }
        if (action === 'help') {
          window.open(_this2.helpUrl);
        }
        return _get(_getPrototypeOf(Dialog.prototype), "getActionProcess", _this2).call(_this2, action);
      }).next(function () {
        if (action === 'submit') {
          _this2.close({
            action: action
          });
          mw.hook('postEdit').fire({
            message: mw.msg('asl-itemlinked')
          });
        }
        return _get(_getPrototypeOf(Dialog.prototype), "getActionProcess", _this2).call(_this2, action);
      }).next(function () {
        if (action === 'close') {
          _this2.close({
            action: action
          });
        }
        return _get(_getPrototypeOf(Dialog.prototype), "getActionProcess", _this2).call(_this2, action);
      });
    }
  }]);
  return Dialog;
}(OO.ui.ProcessDialog);
function _getItemsFieldset2(items) {
  var _this3 = this;
  this.itemSelect = new OO.ui.RadioSelectWidget().connect(this, {
    choose: function choose() {
      return _this3.getActions().setAbilities({
        submit: true
      });
    }
  });

  /**
   * Quick helper function to strip out HTML from a string,
   * which is present in the search result snippets because
   * HTML is used to highlight the searched term.
   *
   * @param {string} str
   * @return {string}
   */
  var stripHTML = function stripHTML(str) {
    return str.replace(/<\/?.+?>/ig, '');
  };
  items.forEach(function (item) {
    var $qItemLink = $('<a>').attr('href', _this3.aslContext.wikidataUrl + '/wiki/' + item.title).attr('target', '_blank').text(item.title);
    var $qItemLabel = $('<span>').text(mw.msg('parentheses-start') + stripHTML(item.titlesnippet) + mw.msg('parentheses-end'));
    var $qItemDesc = $('<span>').attr('style', 'font-style: italic; display: block; padding-right: 5px;').text(stripHTML(item.snippet) || mw.msg('asl-nodescription'));
    _this3.itemSelect.addItems([new OO.ui.RadioOptionWidget({
      label: $('<span>').append($qItemLink, ' ', $qItemLabel, $qItemDesc),
      data: item.title
    })]);
  });
  var description = new OO.ui.Element({
    $content: $('<p>').text(mw.msg('asl-heading-desc'))
  });
  return new OO.ui.FieldsetLayout({
    label: mw.msg('asl-heading'),
    items: [description, this.itemSelect]
  });
}
_defineProperty(Dialog, "name", 'aslDialog');
_defineProperty(Dialog, "size", 'large');
module.exports = Dialog;

},{}]},{},[1]);
