/**
 * @class
 * @property {AutosuggestSitelink} aslContext
 * @property {Array<Object>} items
 */
class Dialog extends OO.ui.ProcessDialog {

	static name = 'aslDialog';
	static size = 'large';

	/**
	 * @param {AutosuggestSitelink} aslContext
	 * @param {Array<Object>} items
	 * @constructor
	 */
	constructor( aslContext, items ) {
		super();
		this.aslContext = aslContext;
		this.items = items;
		this.helpUrl = 'https://meta.wikimedia.org/wiki/Meta:AutosuggestSitelink';

		// These properties are set here instead of using the static keyword above
		//   because mw.msg() isn't available in the static context.
		Dialog.title = mw.msg( 'asl-popuptitle' );
		Dialog.actions = [
			{
				action: 'submit',
				label: mw.msg( 'asl-submit' ),
				flags: [ 'primary', 'progressive' ],
				disabled: true
			},
			{
				action: 'help',
				label: mw.msg( 'asl-help' ),
				icon: 'helpNotice',
				href: this.helpUrl
			},
			{
				action: 'close',
				flags: [ 'safe', 'close' ]
			}
		];

		// Add properties needed by the ES5-based OOUI inheritance mechanism.
		// This roughly simulates OO.inheritClass()
		Dialog.parent = Dialog.super = OO.ui.ProcessDialog;
		OO.initClass( OO.ui.ProcessDialog );
		Dialog.static = Object.create( OO.ui.ProcessDialog.static );
		Object.keys( Dialog ).forEach( ( key ) => {
			Dialog.static[ key ] = Dialog[ key ];
		} );
	}

	/**
	 * @param {...*} args
	 * @override
	 */
	initialize( ...args ) {
		super.initialize( ...args );
		this.itemsFieldset = this.#getItemsFieldset( this.items );
		this.panel = new OO.ui.PanelLayout( { padded: true, expanded: false } );
		this.panel.$element.append( this.itemsFieldset.$element );
		this.$body.append( this.panel.$element );
	}

	/**
	 * Get the fieldset of items with radio buttons.
	 *
	 * @param {Array<Object>} items
	 * @return {OO.ui.FieldsetLayout}
	 */
	#getItemsFieldset( items ) {
		this.itemSelect = new OO.ui.RadioSelectWidget()
			.connect( this, {
				choose: () => this.getActions().setAbilities( { submit: true } )
			} );

		/**
		 * Quick helper function to strip out HTML from a string,
		 * which is present in the search result snippets because
		 * HTML is used to highlight the searched term.
		 *
		 * @param {string} str
		 * @return {string}
		 */
		const stripHTML = ( str ) => str.replace( /<\/?.+?>/ig, '' );

		items.forEach( ( item ) => {
			const $qItemLink = $( '<a>' )
				.attr( 'href', this.aslContext.wikidataUrl + '/wiki/' + item.title )
				.attr( 'target', '_blank' )
				.text( item.title );
			const $qItemLabel = $( '<span>' ).text(
				mw.msg( 'parentheses-start' ) +
				stripHTML( item.titlesnippet ) +
				mw.msg( 'parentheses-end' )
			);
			const $qItemDesc = $( '<span>' )
				.attr( 'style', 'font-style: italic; display: block; padding-right: 5px;' )
				.text( stripHTML( item.snippet ) || mw.msg( 'asl-nodescription' ) );
			this.itemSelect.addItems( [
				new OO.ui.RadioOptionWidget( {
					label: $( '<span>' ).append( $qItemLink, ' ', $qItemLabel, $qItemDesc ),
					data: item.title
				} )
			] );
		} );

		const description = new OO.ui.Element( {
			$content: $( '<p>' ).text( mw.msg( 'asl-heading-desc' ) )
		} );

		return new OO.ui.FieldsetLayout( {
			label: mw.msg( 'asl-heading' ),
			items: [ description, this.itemSelect ]
		} );
	}

	/**
	 * @param {string} action
	 * @return {OO.ui.Process}
	 * @override
	 */
	getActionProcess( action ) {
		return super.getActionProcess( action )
			.next( () => {
				if ( action === 'submit' ) {
					return this.aslContext.submit( this.itemSelect.findSelectedItem().data );
				}
				if ( action === 'help' ) {
					window.open( this.helpUrl );
				}

				return super.getActionProcess( action );
			} )
			.next( () => {
				if ( action === 'submit' ) {
					this.close( { action } );
					mw.hook( 'postEdit' ).fire( {
						message: mw.msg( 'asl-itemlinked' )
					} );
				}

				return super.getActionProcess( action );
			} )
			.next( () => {
				if ( action === 'close' ) {
					this.close( { action } );
				}

				return super.getActionProcess( action );
			} );
	}

}
module.exports = Dialog;
