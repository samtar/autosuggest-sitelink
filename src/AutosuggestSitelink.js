/**
 * @class
 * @property {string} dbName The db name of the current wiki
 * @property {string} page The name of page
 * @property {string} wikidataUrl
 * @property {string} infoUrl Documentation page for this script
 * @property {Object} api The api object to query data from wikidata
 */
class AutosuggestSitelink {

	constructor() {
		this.dbName = mw.config.get( 'wgDBname' );
		this.page = mw.config.get( 'wgPageName' );
		this.title = mw.config.get( 'wgTitle' );
		this.wikidataUrl = 'https://www.wikidata.org';

		// When on testwiki, use Test Wikidata.
		if ( this.dbName === 'testwiki' ) {
			this.wikidataUrl = 'https://test.wikidata.org';
		}

		this.api = new mw.ForeignApi( this.wikidataUrl + '/w/api.php' );
	}

	/**
	 * Check if page has sitelinks related to this wiki,
	 * and if it does, suggest possible sitelinks.
	 */
	checkSitelink() {
		this.api.get( {
			format: 'json',
			action: 'wbgetentities',
			props: 'sitelinks/urls',
			sitefilter: this.dbName,
			sites: this.dbName,
			titles: this.page
		} ).done( ( data ) => {
			// If "-1" key is not found in entities then the page has sitelinks.
			if ( data.entities[ '-1' ] !== undefined ) {
				this.loadTranslations().then(
					this.suggestSiteLink.bind( this )
				);
			}
		} );
	}

	/**
	 * Not found, include link to create new item.
	 *
	 * @return {jQuery.Object}
	 */
	itemsNotFound() {
		const $container = $( '<div>' );

		$container.append(
			$( '<p>' ).text( mw.msg( 'asl-notfound' ) ),
			$( '<a>' ).attr( 'href',
				this.wikidataUrl + '/wiki/Special:NewItem?' +
				new URLSearchParams( {
					site: this.dbName,
					page: this.page,
					label: this.title,
					lang: mw.config.get( 'wgContentLanguage' )
				} )
			).text( mw.msg( 'asl-createnewitem' ) )
				.attr( 'target', '_blank' )
				.attr( 'style', 'margin-top: 30px;' )
		);

		return $container;
	}

	/**
	 * Find possible related wikidata items based on the page title.
	 * If items are found, a dialog is shown with a submission form.
	 */
	suggestSiteLink() {
		this.api.get( {
			format: 'json',
			action: 'query',
			list: 'search',
			srsearch: this.title,
			srprop: 'snippet|titlesnippet'
		} ).then( ( data ) => {
			if ( data.query.searchinfo.totalhits === 0 ) {
				// No sitelinks at all.
				mw.notify( this.itemsNotFound(), {
					autoHideSeconds: 'long',
					tag: 'itemsNotFound'
				} );
				return;
			}

			const Dialog = require( './Dialog.js' );
			const dialogInstance = new Dialog( this, data.query.search );

			// Add the dialog to the window manager
			const windowManager = OO.ui.getWindowManager();
			windowManager.addWindows( [ dialogInstance ] );
			windowManager.openWindow( dialogInstance );
		} );
	}

	/**
	 * Send a POST request to add the sitelink.
	 *
	 * @param {string} item
	 * @return {jQuery.Promise}
	 */
	submit( item ) {
		return this.api.postWithToken( 'csrf', {
			action: 'wbsetsitelink',
			id: item,
			linksite: this.dbName,
			linktitle: this.page
		} );
	}

	/**
	 * Load the translations from the on-wiki messages page.
	 *
	 * @return {jQuery.Deferred}
	 */
	loadTranslations() {
		const dfd = $.Deferred(),
			messagesPage = 'MediaWiki:Gadget-AutosuggestSitelink-messages',
			metaApi = new mw.ForeignApi( 'https://meta.wikimedia.org/w/api.php' ),
			userLang = mw.config.get( 'wgUserLanguage' ),
			langPageEn = `${messagesPage}/en`,
			langPageLocal = `${messagesPage}/${userLang}`,
			titles = [ langPageEn ];
		if ( mw.config.get( 'wgUserLanguage' ) !== 'en' ) {
			// Fetch the translation in the user's language, if not English.
			titles.push( langPageLocal );
		}

		const coreMessagesPromise = metaApi.loadMessagesIfMissing( [
			'parentheses-start',
			'parentheses-end'
		] );
		const aslMessagesPromise = metaApi.get( {
			action: 'query',
			prop: 'revisions',
			titles,
			rvprop: 'content',
			rvslots: 'main',
			format: 'json',
			formatversion: 2
		} ).then( ( resp ) => {
			let messagesLocal = {},
				messagesEn = {};

			resp.query.pages.forEach( ( page ) => {
				let parsedContent;

				if ( page.missing ) {
					return;
				}

				/**
				 * The content model of the messages page is wikitext so that it can be used with
				 * Extension:Translate. Consequently, it's easy to break things, so we do a
				 * try/catch, try some commons fixes, and indicate the likely culprit to the user.
				 */
				let content = page.revisions[ 0 ].slots.main.content;
				try {
					try {
						parsedContent = JSON.parse( content );
					} catch {
						// A common failure reason is because of the HTML that's added for
						// untranslated messages, so remove this and try to parse again.
						// eslint-disable-next-line es-x/no-string-prototype-replaceall
						content = content.replaceAll( '<span lang="en" dir="ltr" class="mw-content-ltr">', '' );
						// eslint-disable-next-line es-x/no-string-prototype-replaceall
						content = content.replaceAll( '</span>', '' );
						parsedContent = JSON.parse( content );
					}
				} catch {
					// If it's still failing, there's something else wrong with the
					// messages (e.g. a double quote within a translated message).
					const metaLink = '<a href="https://meta.wikimedia.org/wiki/">' + page.title + '</a>';
					return OO.ui.alert(
						$(
							'<span>Unable to parse the messages page ' + metaLink + '. ' +
							'There may have been a recent change that contains invalid JSON.</span>'
						),
						{ title: 'Something went wrong' }
					);
				}

				if ( page.title === langPageLocal ) {
					messagesLocal = parsedContent.messages;
				} else {
					messagesEn = parsedContent.messages;
				}
			} );

			mw.messages.set( Object.assign( {}, messagesEn, messagesLocal ) );
		} );
		Promise.all( [ coreMessagesPromise, aslMessagesPromise ] ).then( () => {
			dfd.resolve();
		} );

		return dfd;
	}
}

function init() {
	// Return when not in a content namespace or the article does not exist.
	const nsIds = mw.config.get( 'wgNamespaceIds' );
	const validNamespaces = mw.config.get( 'wgContentNamespaces' )
		// Remove File NS.
		.filter( ( e ) => {
			return e !== nsIds.file;
		} );
	// Add Category NS.
	validNamespaces.push( nsIds.category );
	const ns = mw.config.get( 'wgNamespaceNumber' );
	if ( !validNamespaces.includes( ns ) || mw.config.get( 'wgArticleId' ) === 0 ) {
		return;
	}

	Promise.all( [
		// Resource loader modules
		mw.loader.using( [
			'mediawiki.action.view.postEdit',
			'mediawiki.ForeignApi',
			'mediawiki.Title',
			'oojs-ui-core',
			'oojs-ui-windows'
		] )
	] ).then( () => {
		const autosuggestSitelink = new AutosuggestSitelink();
		autosuggestSitelink.checkSitelink();
	} );
}
$.when( mw.loader.using( [ 'mediawiki.util' ] ), $.ready ).then( function () {
	if ( window.AutosuggestSitelinkDebug ) {
		init();
	} else {
		mw.hook( 'postEdit' ).add( init );
	}
	const link = mw.util.addPortletLink( 'p-tb', '#', 'AutosuggestSiteLink', 'tb-autosuggestsitelink' );
	$( link ).on( 'click', function ( e ) {
		$( link ).children().css( {
			color: '#C4C2C1'
		} );
		setTimeout( function () {
			$( link ).children().css( {
				color: ''
			} );
		}, 3000 );
		init();
		e.preventDefault();
	} );
} );
